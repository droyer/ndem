#pragma once

#include <ncurses.h>

class Block;

class Game
{
	public:
		Game(int width = 9, int height = 9, int bombCount = 10);
		~Game();

		void InitWin();

		void Update();
		void Draw();

		Block* GetBlock(int x, int y);

		bool IsPendingKill();

		void Close();

	private:
		WINDOW* win;
		int width;
		int height;
		int scale;
		Block** blocks;
		int xSelect;
		int ySelect;

		Block* GetSelectBlock();
		
		bool bGenerate;

		void Generate();
		
		int RandomRange(int min, int max);

		bool bLost;

		bool bEnd;

		void End(const char* message);

		void Restart();

		void Init();

		void CheckWin();
		
		int bombCount;

		bool bPendingKill;

		void FreeBlocks();
};
