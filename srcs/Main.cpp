#include <ncurses.h>
#include <stdlib.h>
#include <signal.h>

#include "Game.h"
#include "Block.h"

Game* game = NULL;

static void sighandler(int signum)
{
	if (signum == SIGINT)
	{
		delete game;
		endwin();
		exit(0);
	}
}

int main(int argc, char** argv)
{
	for (int i = 1; i < 32; i++)
		signal(i, sighandler);

	if (argc == 4)
		game = new Game(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
	else
		game = new Game();

	game->InitWin();

	while (!game->IsPendingKill())
	{
		game->Draw();
		game->Update();
	}
	delete game;
	endwin();

	return 0;
}
